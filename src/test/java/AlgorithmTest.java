import game.Algorithm;
import game.Coordinate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AlgorithmTest {

    private Algorithm algorithm;

    @Before
    public void createObjectAlgorithm() {
        algorithm = new Algorithm();
    }

    @Test
    public void testExistFieldGame() {
        algorithm.getFieldGame(3, 5);
    }

    @Test
    public void testReturnFieldGame() {
        int[][] array = algorithm.getFieldGame(3, 5);
    }

    @Test
    public void testFieldGameSizeNot0x0() {
        int[][] array = algorithm.getFieldGame(4, 6);
        Assert.assertNotNull(array);
    }

    @Test
    public void testFieldGameSize8x8() {
        int[][] array = algorithm.getFieldGame(8, 8);
        Assert.assertEquals("Wrong length of array!",8, array.length);
        Assert.assertEquals("Wrong length of string array!",8, array[0].length);
    }

    @Test
    public void testFieldGameSizeAxB() {
        int a = 2;
        int b = 3;
        int[][] array = algorithm.getFieldGame(a, b);
        Assert.assertEquals("Wrong length of array!",a, array.length);
        Assert.assertEquals("Wrong length of string array!",b, array[0].length);
    }

    @Test
    public void testFieldGameNot0() {
        int[][] array = algorithm.getFieldGame(8, 8);
        for (int i = 0; i < array.length; i++)
        {
            for (int j = 0; j < array[i].length; j++)
            {
                Assert.assertNotEquals(0, array[i][j]);
            }
        }
    }

    @Test
    public void testGetAandB() {
        int[][] array = new int[3][4];
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertNotNull(cordinate);
    }
//----------------------------------------------------------------------------------------------------------------------
    //---
    //---
    //---
    @Test
    public void testMissingCoordinates() {
        int[][] array = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
        int a = -1;
        int b = -1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }
//----------------------------------------------------------------------------------------------------------------------
    //--x-xx-
    @Test
    public void testFindCoordinatesHorizontallyLength3Version1() {
        int[][] array = {{0, 0, 0, 0, 0, 0}, {0, 0, 1, 0, 1, 1}, {0, 0, 0, 0, 0, 0}};
        int a = 1;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //--xx-x-
    @Test
    public void testFindCoordinatesHorizontallyLength3Version2() {
        int[][] array = {{0, 0, 0, 0, 0, 0}, {0, 0, 1, 1, 0, 1}, {0, 0, 0, 0, 0, 0}};
        int a = 1;
        int b = 5;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-xx-
    //---x
    @Test
    public void testFindCoordinatesHorizontallyLength3Version3() {
        int[][] array = {{0, 0, 0, 0, 0, 0}, {0, 1, 1, 0, 0, 0}, {0, 0, 0, 1, 0, 0}};
        int a = 2;
        int b = 3;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //---x
    //-xx-
    @Test
    public void testFindCoordinatesHorizontallyLength3Version4() {
        int[][] array = {{0, 0, 0, 0, 0, 1}, {0, 0, 0, 1, 1, 0}, {0, 0, 0, 0, 0, 0}};
        int a = 0;
        int b = 5;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //x---
    //-xx-
    @Test
    public void testFindCoordinatesHorizontallyLength3Version5() {
        int[][] array = {{0, 0, 1, 0, 0, 0}, {0, 0, 0, 1, 1, 0}, {0, 0, 0, 0, 0, 0}};
        int a = 0;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-xx-
    //x---
    @Test
    public void testFindCoordinatesHorizontallyLength3Version6() {
        int[][] array = {{0, 0, 0, 0, 0, 0}, {0, 0, 0, 1, 1, 0}, {0, 0, 1, 0, 0, 0}};
        int a = 2;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //x-x
    //-x-
    @Test
    public void testFindCoordinatesHorizontallyLength3Version7() {
        int[][] array = {{1, 0, 1}, {0, 1, 0}};
        int a = 1;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x-
    //x-x
    @Test
    public void testFindCoordinatesHorizontallyLength3Version8() {
        int[][] array = {{0, 1, 0}, {1, 0, 1}};
        int a = 0;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x-
    //x--
    //-x-
    @Test
    public void testFindCoordinatesVerticallyLength3Version1() {
        int[][] array = {{0, 1, 0}, {1, 0, 0}, {0, 1, 0}};
        int a = 1;
        int b = 0;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x-
    //--x
    //-x-
    @Test
    public void testFindCoordinatesVerticallyLength3Version2() {
        int[][] array = {{0, 1, 0}, {0, 0, 1}, {0, 1, 0}};
        int a = 1;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x-
    //-x-
    //---
    //-x-
    @Test
    public void testFindCoordinatesVerticallyLength3Version3() {
        int[][] array = {{0, 0, 0}, {0, 0, 0}, {0, 1, 0}, {0, 1, 0}, {0, 0, 0}, {0, 1, 0}};
        int a = 5;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x-
    //---
    //-x-
    //-x-
    @Test
    public void testFindCoordinatesVerticallyLength3Version4() {
        int[][] array = {{0, 0, 0}, {0, 0, 0}, {0, 1, 0}, {0, 0, 0}, {0, 1, 0}, {0, 1, 0}};
        int a = 2;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x-
    //-x-
    //x--
    @Test
    public void testFindCoordinatesVerticallyLength3Version5() {
        int[][] array = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 1, 0}, {0, 1, 0}, {1, 0, 0}};
        int a = 5;
        int b = 0;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x-
    //-x-
    //--x
    @Test
    public void testFindCoordinatesVerticallyLength3Version6() {
        int[][] array = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 1, 0}, {0, 1, 0}, {0, 0, 1}};
        int a = 5;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //x--
    //-x-
    //-x-
    @Test
    public void testFindCoordinatesVerticallyLength3Version7() {
        int[][] array = {{0, 0, 0}, {0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {0, 1, 0}, {0, 0, 0}};
        int a = 2;
        int b = 0;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //--x
    //-x-
    //-x-
    @Test
    public void testFindCoordinatesVerticallyLength3Version8() {
        int[][] array = {{0, 0, 0}, {0, 0, 0}, {0, 0, 1}, {0, 1, 0}, {0, 1, 0}, {0, 0, 0}};
        int a = 2;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }
//----------------------------------------------------------------------------------------------------------------------
    //--x--
    //xx-xx
    //--x--
    //--x--
    @Test
    public void testFindCoordinatesHorizontallyLength5Max7Version1() {
        int[][] array = {{0, 0, 1, 0, 0}, {1, 1, 0, 1, 1}, {0, 0, 1, 0, 0}, {0, 0, 1, 0, 0}};
        int a = 0;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //--x--
    //--x--
    //xx-xx
    //--x--
    @Test
    public void testFindCoordinatesHorizontallyLength5Max7Version2() {
        int[][] array = {{0, 0, 1, 0, 0}, {0, 0, 1, 0, 0}, {1, 1, 0, 1, 1}, {0, 0, 1, 0, 0}};
        int a = 3;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //--x--
    //--x--
    //-x-xx
    //--x--
    //--x--
    @Test
    public void testFindCoordinatesVerticallyLength5Max7Version1() {
        int[][] array = {{0, 1, 0, 0}, {0, 1, 0, 0}, {1, 0, 1, 1}, {0, 1, 0, 0}, {0, 1, 0, 0}};
        int a = 2;
        int b = 0;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //--x-
    //--x-
    //xx-x
    //--x-
    //--x-
    @Test
    public void testFindCoordinatesVerticallyLength5MaxElements7Version2() {
        int[][] array = {{0, 0, 1, 0}, {0, 0, 1, 0}, {1, 1, 0, 1}, {0, 0, 1, 0}, {0, 0, 1, 0}};
        int a = 2;
        int b = 3;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }
//----------------------------------------------------------------------------------------------------------------------

    //--x-
    //xx-x
    //--x-
    //--x-
    @Test
    public void testFindCoordinatesMaxElements6Version1() {
        int[][] array = {{0, 0, 1, 0}, {1, 1, 0, 1}, {0, 0, 1, 0}, {0, 0, 1, 0}};
        int a = 0;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x--
    //x-xx
    //-x--
    //-x--
    @Test
    public void testFindCoordinatesMaxElements6Version2() {
        int[][] array = {{0, 1, 0, 0}, {1, 0, 1, 1}, {0, 1, 0, 0}, {0, 1, 0, 0}};
        int a = 0;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //--x-
    //--x-
    //xx-x
    //--x-
    @Test
    public void testFindCoordinatesMaxElements6Version3() {
        int[][] array = {{0, 0, 1, 0}, {0, 0, 1, 0}, {1, 1, 0, 1}, {0, 0, 1, 0}};
        int a = 3;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x-
    //-x-
    //x-xx
    //-x-
    @Test
    public void testFindCoordinatesMaxElements6Version4() {
        int[][] array = {{0, 1, 0, 0}, {0, 1, 0, 0}, {1, 0, 1, 1}, {0, 1, 0, 0}};
        int a = 3;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }
//----------------------------------------------------------------------------------------------------------------------
    //--x--
    //xx-xx
    @Test
    public void testFindCoordinatesMaxElements5Version1() {
        int[][] array = {{0, 0, 1, 0, 0}, {1, 1, 0, 1, 1}};
        int a = 0;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //xx-xx
    //--x--
    @Test
    public void testFindCoordinatesMaxElements5Version2() {
        int[][] array = {{1, 1, 0, 1, 1}, {0, 0, 1, 0, 0}};
        int a = 1;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x
    //-x
    //x-
    //-x
    //-x
    @Test
    public void testFindCoordinatesMaxElements5Version3() {
        int[][] array = {{0, 1}, {0, 1}, {1, 0}, {0, 1}, {0, 1}};
        int a = 2;
        int b = 0;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //x-
    //x-
    //-x
    //x-
    //x-
    @Test
    public void testFindCoordinatesMaxElements5Version4() {
        int[][] array = {{1, 0}, {1, 0}, {0, 1}, {1, 0}, {1, 0}};
        int a = 2;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x-
    //-x-
    //x-xx
    @Test
    public void testFindCoordinatesMaxElements5Version5() {
        int[][] array = {{0, 1, 0, 0}, {0, 1, 0, 0}, {1, 0, 1, 1}};
        int a = 2;
        int b = 0;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //x--
    //x--
    //-xx
    //x--
    @Test
    public void testFindCoordinatesMaxElements5Version6() {
        int[][] array = {{1, 0, 0}, {1, 0, 0}, {0, 1, 1}, {1, 0, 0}};
        int a = 3;
        int b = 0;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //x-xx
    //-x--
    //-x--
    @Test
    public void testFindCoordinatesMaxElements5Version7() {
        int[][] array = {{1, 0, 1, 1}, {0, 1, 0, 0}, {0, 1, 0, 0}};
        int a = 0;
        int b = 0;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //x--
    //-xx
    //x--
    //x--
    @Test
    public void testFindCoordinatesMaxElements5Version8() {
        int[][] array = {{1, 0, 0}, {0, 1, 1}, {1, 0, 0}, {1, 0, 0}};
        int a = 0;
        int b = 0;
        Coordinate cordinate =algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //--x
    //--x
    //xx-
    //--x
    @Test
    public void testFindCoordinatesMaxElements5Version9() {
        int[][] array = {{0, 0, 1}, {0, 0, 1}, {1, 1, 0}, {0, 0, 1}};
        int a = 3;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //--x-
    //--x-
    //xx-x
    @Test
    public void testFindCoordinatesMaxElements5Version10() {
        int[][] array = {{0, 0, 1, 0}, {0, 0, 1, 0}, {1, 1, 0, 1}};
        int a = 2;
        int b = 3;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //xx-x
    //--x-
    //--x-
    @Test
    public void testFindCoordinatesMaxElements5Version11() {
        int[][] array = {{1, 1, 0, 1}, {0, 0, 1, 0}, {0, 0, 1, 0}};
        int a = 0;
        int b = 3;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //--x
    //xx-
    //--x
    //--x
    @Test
    public void testFindCoordinatesMaxElements5Version12() {
        int[][] array = {{0, 0, 1}, {1, 1, 0}, {0, 0, 1}, {0, 0, 1}};
        int a = 0;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x-
    //x-x
    //-x-
    //-x-
    @Test
    public void testFindCoordinatesMaxElements5Version13() {
        int[][] array = {{0, 1, 0}, {1, 0, 1}, {0, 1, 0}, {0, 1, 0}};
        int a = 0;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x-
    //-x-
    //x-x
    //-x-
    @Test
    public void testFindCoordinatesMaxElements5Version14() {
        int[][] array = {{0, 1, 0}, {0, 1, 0}, {1, 0, 1}, {0, 1, 0}};
        int a = 3;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x--
    //x-xx
    //-x--
    @Test
    public void testFindCoordinatesMaxElements5Version15() {
        int[][] array = {{0, 1, 0, 0}, {1, 0, 1, 1}, {0, 1, 0, 0}};
        int a = 1;
        int b = 0;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //--x-
    //xx-x
    //--x-
    @Test
    public void testFindCoordinatesMaxElements5Version16() {
        int[][] array = {{0, 0, 1, 0,}, {1, 1, 0, 1}, {0, 0, 1, 0,}};
        int a = 1;
        int b = 3;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }
//----------------------------------------------------------------------------------------------------------------------
    //-x
    //x-
    //-x
    //-x
    @Test
    public void testFindCoordinatesVerticallyLength4Version1() {
        int[][] array = {{0, 1}, {1, 0}, {0, 1}, {0, 1}};
        int a = 1;
        int b = 0;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //x-
    //-x
    //x-
    //x-
    @Test
    public void testFindCoordinatesVerticallyLength4Version2() {
        int[][] array = {{1, 0}, {0, 1}, {1, 0}, {1, 0}};
        int a = 1;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //x-
    //x-
    //-x
    //x-
    @Test
    public void testFindCoordinatesVerticallyLength4Version3() {
        int[][] array = {{1, 0}, {1, 0}, {0, 1}, {1, 0}};
        int a = 2;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x
    //-x
    //x-
    //-x
    @Test
    public void testFindCoordinatesVerticallyLength4Version4() {
        int[][] array = {{0, 1}, {0, 1}, {1, 0}, {0, 1}};
        int a = 2;
        int b = 0;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //--x-
    //xx-x
    @Test
    public void testFindCoordinatesHorizontallyLength4Version1() {
        int[][] array = {{0, 0, 1, 0}, {1, 1, 0, 1}};
        int a = 0;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //xx-x
    //--x-
    @Test
    public void testFindCoordinatesHorizontallyLength4Version2() {
        int[][] array = {{1, 1, 0, 1}, {0, 0, 1, 0}};
        int a = 1;
        int b = 2;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //-x--
    //x-xx
    @Test
    public void testFindCoordinatesHorizontallyLength4Version3() {
        int[][] array = {{0, 1, 0, 0}, {1, 0, 1, 1}};
        int a = 0;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }

    //x-xx
    //-x--
    @Test
    public void testFindCoordinatesHorizontallyLength4Version4() {
        int[][] array = {{1, 0, 1, 1}, {0, 1, 0, 0}};
        int a = 1;
        int b = 1;
        Coordinate cordinate = algorithm.getCoordinates(array);
        Assert.assertEquals(a, cordinate.getA());
        Assert.assertEquals(b, cordinate.getB());
    }
//----------------------------------------------------------------------------------------------------------------------
}