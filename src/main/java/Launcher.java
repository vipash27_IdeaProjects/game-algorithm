import game.Algorithm;
import game.Coordinate;

public class Launcher {
    public static void main(String[] args) {
        Algorithm algoritm = new Algorithm();
        int[][] array = {{0, 0, 1, 0, 0, 0, 0, 0, 0}, {1, 1, 0, 1, 1, 0, 0, 0, 1}, {0, 0, 0, 0, 0, 0, 0, 0, 1}, {0, 0, 0, 1, 0, 0, 0, 1, 0}, {0, 1, 1, 0, 1, 1, 0, 0, 1}, {0, 0, 0, 1, 0, 0, 0, 0, 1}, {0, 0, 0, 1, 0, 0, 0, 0, 0}};
        Coordinate coordinate = algoritm.getCoordinates(array);

        System.out.println(coordinate.getA());
        System.out.println(coordinate.getB());

    }
}
