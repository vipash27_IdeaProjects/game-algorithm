package game;

public class Algorithm {

    private int[][] fieldGame = new int[8][8];

    public int[][] getFieldGame(int a, int b) {
        fieldGame = new int[a][b];
        setFieldGame();
        return fieldGame;
    }

    private void setFieldGame() {
        for (int i = 0; i < fieldGame.length; i++)
        {
            for (int j = 0; j < fieldGame[i].length; j++)
            {
                fieldGame[i][j] = 1;
            }
        }
    }

    public Coordinate getCoordinates(int[][] array) {
        int a = -1;
        int b = -1;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 1; k < 7; k++)
                {
                    //--x--
                    //xx-xx
                    //--x--
                    //--x--
                    if (i < array.length - 3 && j > 1 && j < array[i].length - 2 && array[i][j] == k
                            && array[i + 1][j - 2] == k && array[i + 1][j - 1] == k && array[i + 1][j + 1] == k && array[i + 1][j + 2] == k && array[i + 2][j] == k && array[i + 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //--x--
                    //--x--
                    //xx-xx
                    //--x--
                    else if (i > 2 && j > 1 && j < array[i].length - 2 && array[i][j] == k
                            && array[i - 1][j - 2] == k && array[i - 1][j - 1] == k && array[i - 1][j - 1] == k && array[i - 1][j - 2] == k && array[i - 2][j] == k && array[i - 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x--
                    //-x--
                    //x-xx
                    //-x--
                    //-x--
                    if (i < array.length - 2 && i > 1 && j < array[i].length - 3 && array[i][j] == k
                            && array[i + 1][j + 1] == k && array[i + 2][j + 1] == k && array[i - 1][j + 1] == k && array[i - 2][j + 1] == k && array[i][j + 2] == k && array[i][j + 3] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //--x-
                    //--x-
                    //xx-x
                    //--x-
                    //--x-
                    if (i > 1 && i < array.length - 2 && j > 2 && array[i][j] == k
                            && array[i + 1][j - 1] == k && array[i + 2][j - 1] == k && array[i - 1][j - 1] == k && array[i - 2][j - 1] == k && array[i][j - 2] == k && array[i][j - 3] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                }
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 1; k < 7; k++)
                {
                    //--x-
                    //xx-x
                    //--x-
                    //--x-
                    if (i < array.length - 3 && j > 1 && j < array[i].length - 1 && array[i][j] == k
                            && array[i + 1][j - 2] == k && array[i + 1][j - 1] == k && array[i + 1][j + 1] == k && array[i + 2][j] == k && array[i + 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x--
                    //x-xx
                    //-x--
                    //-x--
                    if (i < array.length - 3 && j > 0 && j < array[i].length - 2 && array[i][j] == k
                            && array[i + 1][j - 1] == k && array[i + 1][j + 1] == k && array[i + 1][j + 2] == k && array[i + 2][j] == k && array[i + 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //--x-
                    //--x-
                    //xx-x
                    //--x-
                    if (i > 2 && j > 1 && j < array[i].length - 1 && array[i][j] == k
                            && array[i - 1][j - 2] == k && array[i - 1][j - 1] == k && array[i - 1][j + 1] == k && array[i - 2][j] == k && array[i - 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x--
                    //-x--
                    //x-xx
                    //-x--
                    if (i > 2 && j > 0 && j < array[i].length - 2 && array[i][j] == k
                            && array[i - 1][j - 1] == k && array[i - 1][j + 1] == k && array[i - 1][j + 2] == k && array[i - 2][j] == k && array[i - 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                }
            }
        }

        for (int i = 0; i < array.length; i++)
        {
            for (int j = 0; j < array[i].length; j++)
            {
                for (int k = 1; k < 7; k++)
                {
                    //--x--
                    //xx-xx
                    if (i < array.length - 1 && j > 1 && j < array[i].length - 2 && array[i][j] == k && array[i + 1][j - 2] == k && array[i + 1][j - 1] == k && array[i + 1][j + 1] == k && array[i + 1][j + 2] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //xx-xx
                    //--x--
                    else if (i > 0 && j > 1 && j < array[i].length - 2 && array[i][j] == k && array[i - 1][j - 2] == k && array[i - 1][j - 1] == k && array[i - 1][j + 1] == k && array[i - 1][j + 2] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //-x-
                    //x--
                    //-x-
                    //-x-
                    else if (i > 1 && i < array.length - 2 && j < array[i].length - 1 && array[i][j] == k && array[i - 1][j + 1] == k && array[i - 2][j + 1] == k && array[i + 1][j + 1] == k && array[i + 2][j + 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //-x-
                    //--x
                    //-x-
                    //-x-
                    else if (i > 1 && i < array.length - 2 && j > 0 && array[i][j] == k && array[i - 1][j - 1] == k && array[i - 2][j - 1] == k && array[i + 1][j - 1] == k && array[i + 2][j - 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //-x-
                    //x-xx
                    if (i > 1 && j < array[i].length - 3 && array[i][j] == k
                            && array[i - 1][j + 1] == k && array[i - 2][j + 1] == k && array[i][j + 2] == k && array[i][j + 3] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //x--
                    //x--
                    //-xx
                    //x--
                    if (i > 2 && j < array[i].length - 2 && array[i][j] == k
                            && array[i - 1][j + 1] == k && array[i - 1][j + 2] == k && array[i - 2][j] == k && array[i - 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //x-xx
                    //-x--
                    //-x--
                    if (i < array.length - 2 && j < array[i].length - 3 && array[i][j] == k
                            && array[i + 1][j + 1] == k && array[i + 2][j + 1] == k && array[i][j + 2] == k && array[i][j + 3] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //x--
                    //-xx
                    //x--
                    //x--
                    if (i < array.length - 3 && j < array[i].length - 2 && array[i][j] == k
                            && array[i + 1][j + 1] == k && array[i + 1][j + 2] == k && array[i + 2][j] == k && array[i + 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //--x
                    //--x
                    //xx-
                    //--x
                    if (i > 2 && j > 1 && array[i][j] == k
                            && array[i - 1][j - 1] == k && array[i - 1][j - 2] == k && array[i - 2][j] == k && array[i - 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //--x-
                    //--x-
                    //xx-x
                    if (i > 1 && j > 2 && array[i][j] == k
                            && array[i - 1][j - 1] == k && array[i - 2][j - 1] == k && array[i][j - 2] == k && array[i][j - 3] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //xx-x
                    //--x-
                    //--x-
                    if (i < array.length - 2 && j > 2 && array[i][j] == k
                            && array[i + 1][j - 1] == k && array[i + 2][j - 1] == k && array[i][j - 2] == k && array[i][j - 3] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //--x
                    //xx-
                    //--x
                    //--x
                    if (i < array.length - 3 && j > 1 && array[i][j] == k
                            && array[i + 1][j - 1] == k && array[i + 1][j - 2] == k && array[i + 2][j] == k && array[i + 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //x-x
                    //-x-
                    //-x-
                    if (i < array.length - 3 && j > 0 && j < array[i].length - 1 && array[i][j] == k
                            && array[i + 1][j - 1] == k && array[i + 1][j + 1] == k && array[i + 2][j] == k && array[i + 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //-x-
                    //x-x
                    //-x-
                    if (i > 2 && j > 0 && j < array[i].length - 1 && array[i][j] == k
                            && array[i - 1][j - 1] == k && array[i - 1][j + 1] == k && array[i - 2][j] == k && array[i - 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x--
                    //x-xx
                    //-x--
                    if (i > 0 && i < array.length - 1 && j < array[i].length - 3 && array[i][j] == k
                            && array[i - 1][j + 1] == k && array[i + 1][j + 1] == k && array[i][j + 2] == k && array[i][j + 3] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //--x-
                    //xx-x
                    //--x-
                    if (i > 0 && i < array.length - 1 && j > 2 && array[i][j] == k
                            && array[i - 1][j - 1] == k && array[i + 1][j - 1] == k && array[i][j - 2] == k && array[i][j - 3] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                }
            }
        }

        for (int i = 0; i < array.length; i++)
        {
            for (int j = 0; j < array[i].length; j++)
            {
                for (int k = 1; k < 7; k++)
                {
                    //-x
                    //x-
                    //-x
                    //-x
                    if (i > 0 && i < array.length - 2 && j < array[i].length - 1 && array[i][j] == k && array[i - 1][j + 1] == k && array[i + 1][j + 1] == k && array[i + 2][j + 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //x-
                    //-x
                    //x-
                    //x-
                    else if (i > 0 && i < array.length - 2 && j > 0 && array[i][j] == k && array[i - 1][j - 1] == k && array[i + 1][j - 1] == k && array[i + 2][j - 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //x-
                    //x-
                    //-x
                    //x-
                    else if (i > 1 && i < array.length - 1 && j > 0 && array[i][j] == k && array[i - 1][j - 1] == k && array[i - 2][j - 1] == k && array[i + 1][j - 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x
                    //-x
                    //x-
                    //-x
                    else if (i > 1 && i < array.length - 1 && j < array[i].length - 1 && array[i][j] == k && array[i - 1][j + 1] == k && array[i - 2][j + 1] == k && array[i + 1][j + 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //--x-
                    //xx-x
                    if (i < array.length - 1 && j > 1 && j < array[i].length - 1 && array[i][j] == k && array[i + 1][j - 2] == k && array[i + 1][j - 1] == k && array[i + 1][j + 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //xx-x
                    //--x-
                    else if (i > 0 && j > 1 && j < array[i].length - 1 && array[i][j] == k && array[i - 1][j - 2] == k && array[i - 1][j - 1] == k && array[i - 1][j + 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x--
                    //x-xx
                    if (i < array.length - 1 && j > 0 && j < array[i].length - 2 && array[i][j] == k && array[i + 1][j - 1] == k && array[i + 1][j + 1] == k && array[i + 1][j + 2] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //x-xx
                    //-x--
                    else if (i > 0 && j > 0 && j < array[i].length - 2 && array[i][j] == k && array[i - 1][j - 1] == k && array[i - 1][j + 1] == k && array[i - 1][j + 2] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                }
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 1; k < 7; k++)
                {
                    //-xx-x-
                    if(j > 2 && array[i][j] == k && array[i][j - 2] == k && array[i][j - 3] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-xx-
                    else if (j < array[i].length - 3 && array[i][j] == k && array[i][j + 2] == k && array[i][j + 3] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-xx-
                    //---x
                    else if (i > 0 && j > 1 && array[i][j] == k && array[i - 1][j - 1] == k && array[i - 1][j - 2] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //---x
                    //-xx-
                    else if (i < array.length - 1 && j > 1 && array[i][j] == k && array[i + 1][j - 1] == k && array[i + 1][j - 2] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //x---
                    //-xx-
                    else if (i < array.length - 1 && j < array[i].length - 2 && array[i][j] == k && array[i + 1][j + 1] == k && array[i + 1][j + 2] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-xx-
                    //x---
                    else if (i > 0 && j < array[i].length - 2 && array[i][j] == k && array[i - 1][j + 1] == k && array[i - 1][j + 2] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //x-x
                    //-x-
                    else if (i > 0 && j > 0 && j < array[i].length - 1 && array[i][j] == k && array[i - 1][j - 1] == k && array[i - 1][j + 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //x-x
                    else if (i < array.length - 1 && j > 0 && j < array[i].length - 1 && array[i][j] == k && array[i + 1][j - 1] == k && array[i + 1][j + 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //x--
                    //-x-
                    else if (i > 0 && i < array.length - 1 && j < array[i].length - 1 && array[i][j] == k && array[i - 1][j + 1] == k && array[i + 1][j + 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //--x
                    //-x-
                    else if (i > 0 && i < array.length - 1 && j > 0 && array[i][j] == k && array[i - 1][j - 1] == k && array[i + 1][j - 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //-x-
                    //---
                    //-x-
                    else if (i > 2 && array[i][j] == k && array[i - 2][j] == k && array[i - 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //---
                    //-x-
                    //-x-
                    else if (i < array.length - 3 && array[i][j] == k && array[i + 2][j] == k && array[i + 3][j] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //-x-
                    //x--
                    else if (i > 1 && j < array[i].length - 1 && array[i][j] == k && array[i - 1][j + 1] == k && array[i - 2][j + 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //-x-
                    //-x-
                    //--x
                    else if (i > 1 && j > 0 && array[i][j] == k && array[i - 1][j - 1] == k && array[i - 2][j - 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //x--
                    //-x-
                    //-x-
                    else if (i < array.length - 2 && j < array[i].length - 1 && array[i][j] == k && array[i + 1][j + 1] == k && array[i + 2][j + 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                    //--x
                    //-x-
                    //-x-
                    else if (i < array.length - 2 && j > 0 && array[i][j] == k && array[i + 1][j - 1] == k && array[i + 2][j - 1] == k)
                    {
                        a = i;
                        b = j;
                        return new Coordinate(a, b);
                    }
                }
            }
        }
        return new Coordinate(a, b);
    }

    public void getDecided() {

    }
}
