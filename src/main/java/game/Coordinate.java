package game;

public class Coordinate {

    private int a;
    private int b;

    public Coordinate(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

}
